#include "SymbolTable.hpp"
#include "Variable.hpp"

#include <string>
#include <stdexcept>
#include <utility>
#include <vector>
#include <map>

Symbol::Symbol() : Symbol(VariableType::REFERENCE) {
}

Symbol::Symbol(VariableType type) {
    this->overload   = false;
    this->definition = false;

    switch (type) {
        case VariableType::OVERLOAD:
            this->overload = true;
        case VariableType::DEFINITION:
            this->definition = true;
            break;
    }

    if (this->definition) {
        value = new Variable;
    }
    else {
        value = NULL;
    }
}

Symbol::~Symbol() {
    if (definition) {
        delete value;
    }
}

void Symbol::aliasTo(Symbol& val) {
    // If we are trying to alias to an undefined symbol or trying to alias a
    // definition, do nothing
    if (!val.definition || this->definition) return;

    // Alias the values
    this->value = val.value;
}

bool Symbol::isDefinition() {
    return definition;
}

bool Symbol::isOverload() {
    return overload;
}

Variable& Symbol::getValue() {
    return *value;
}

void Symbol::promoteToDefinition() {
    // This assumes it isnt already a definition
    definition = true;
    value = new Variable;
}

void Symbol::promoteToOverload() {
    // This assumes it isnt already a definition
    definition = true;
    overload   = true;
    value = new Variable;
}

SymbolTable::SymbolTable() {
    root   = this;
    parent = NULL;

    isFunctionExpression   = false;
    isFunctionExpressionCT = false;

    isClassScope = false;
}

SymbolTable::~SymbolTable() {
    for (SymbolTable* child : children) {
        delete child;
    }
}

SymbolTable* SymbolTable::newScope() {
    SymbolTable* child = new SymbolTable();
    child->parent = this;
    children.push_back(child);
    return child;
}

SymbolTable* SymbolTable::exitScope() {
    return parent;
}

SymbolTable* SymbolTable::getRoot() {
    return root;
}

SymbolTable::iterator SymbolTable::getRaw(std::string& key) {
    // If a sybol with that name exists in this scope, return it
    auto test = data.lower_bound(key);
    if (test != data.end()) {
        return test;
    }

    // Else, check the parent scope
    if (parent) {
        return parent->getRaw(key);
    }

    // If no parent scope, then the variable does not exist
    return data.end();
}

SymbolTable::iterator SymbolTable::get(std::string& key) {
    if (data.count(key)) {
        // If the identifier was already used in this scope
        return data.lower_bound(key);
    }

    // Get any existing symbol in a parent scope first
    auto existing = getRaw(key);

    // Else we are going to create a new symbol in this scope (even if it was
    // defined in a parent scope)
    auto ret = data.emplace(key, VariableType::REFERENCE).first;

    if (existing != data.end()) {
        // If this was symbol was used in a parent scope, alias
        Symbol& retVal      = ret->second;
        Symbol& existingVal = existing->second;

        retVal.aliasTo(existingVal);
    }

    return ret;
}

SymbolTable::iterator SymbolTable::define(std::string& key) {
    // Check if this symbol already exists in this scope
    if (data.count(key)) {
        auto existing = data.lower_bound(key);
        Symbol& existingVal = existing->second;

        // If its a definition, this is a redeclaration which is an error
        if (existingVal.isDefinition()) throw std::out_of_range("SymbolTable::define");

        // else we just want to promote it to a definition
        existingVal.promoteToDefinition();
        return existing;
    }

    // If we attempt to define 'this' als throw a out_of_rang
    if (key == "this") {
        throw std::out_of_range("SymbolTable::define");
    }

    // Define the new symbol
    auto ret = data.emplace(key, VariableType::DEFINITION).first;
    Symbol& retVal = ret->second;

    // Alias all variables in children scopes to ret (link forward references)
    aliasAllForwardRefs(key, retVal);

    return ret;
}

SymbolTable::iterator SymbolTable::overload(std::string& key) {
    // If this symbol is overloaded in this scope, fetch it
    if (data.count(key) && data[key].isDefinition()) {
        auto existing = data.lower_bound(key);
        Symbol& existingVal = existing->second;

        // If it was overloaded earlier, thats fine, just return is
        if (existingVal.isOverload()) return existing;

        // If it was *defined* earlier, this is redeclaration.
        if (existingVal.isDefinition()) throw std::out_of_range("SymbolTable::overload");

        // Otherwise promote it to an overload
        existingVal.promoteToOverload();
        return existing;
    }

    // Otherwise define a new symbol
    auto ret = data.emplace(key, VariableType::OVERLOAD).first;
    Symbol& retVal = ret->second;

    // We still need to alias this to the forward references
    aliasAllForwardRefs(key, retVal);

    return ret;
}

void SymbolTable::aliasAllForwardRefs(std::string& key, Symbol& val) {
    if (data.count(key)) {
        data[key].aliasTo(val);
    }

    for (auto child : children) {
        child->aliasAllForwardRefs(key, val);
    }
}
