#include "Type.hpp"

#include <vector>

Type::Type() {
    this->category = UNDEFINED;
}

Type::Type(PrimitiveType prim) {
    this->category = PRIMITIVE;
    this->prim     = prim;
}

Type Type::referenceType(Type modified) {
    Type ret;
    ret.category  = REFERENCE;
    ret.signature = std::vector<Type>();
    ret.signature.push_back(modified);

    return ret;
}

Type Type::constType(Type modified) {
    Type ret;
    ret.category  = CONST;
    ret.signature = {modified};
    ret.signature = std::vector<Type>();
    ret.signature.push_back(modified);

    return ret;
}

Type Type::functionType(bool compileTime, std::vector<Type>& signature) {
    Type ret;
    ret.category  = compileTime ? CT_FUNCTION : FUNCTION;
    ret.signature = signature;

    return ret;
}

Type Type::functionType(bool compileTime, std::vector<Type>&& signature) {
    Type ret;
    ret.category  = compileTime ? CT_FUNCTION : FUNCTION;
    ret.signature = std::move(signature);

    return ret;
}

// This is kind of arbitrary to be honest, but I need it for this to be a key
bool operator<(const Type& lhs, const Type& rhs) {
    // If they are not the same category, compare by category
    if (lhs.category != rhs.category) return lhs.category < rhs.category;

    // undefined types should always return false (like NaN)
    if (lhs.category == Type::UNDEFINED) {
        return false;
    }

    // Primitive types compare by their type enum
    if (lhs.category == Type::PRIMITIVE) {
        return lhs.prim < rhs.prim;
    }

    // Compare by the type they are modifying
    if (lhs.category == Type::REFERENCE || lhs.category == Type::CONST) {
        return lhs.signature.front() < rhs.signature.front();
    }

    // We have a function type, first compare by length of signature
    if (lhs.signature.size() != rhs.signature.size()) {
        return lhs.signature.size() < rhs.signature.size();
    }

    // Otherwise try to compare by the types in the signatures
    for (int i = 0; i < lhs.signature.size(); ++i) {
        if (lhs.signature[i] != rhs.signature[i]) {
            return lhs.signature[i] < rhs.signature[i];
        }
    }

    // Else these types are equal
    return false;
}

// Note that we can't define this in terms of operator< as we use this to
// define operator< !!
bool operator==(const Type& lhs, const Type& rhs) {
    if (lhs.category != rhs.category)    return false;
    if (lhs.category == Type::UNDEFINED) return false;
    if (lhs.category == Type::PRIMITIVE) return lhs.prim == rhs.prim;

    // else
    // This might actually not work 100% of the time depending on how == works
    // for vectors, it *should* be fine though...
    return lhs.signature == rhs.signature;
}

bool operator!=(const Type& lhs, const Type& rhs) { return !(lhs == rhs);}
