#include "Variable.hpp"
#include "ParseUnit.hpp"
#include "Type.hpp"

#include <stdexcept>

Variable::Variable() {
    definition = NULL;
}

void Variable::useDefinition(ParseUnit& pu) {
    // This should never happen, but you know, better to be safe
    if (this->definition) {
        throw std::invalid_argument("Variable::useDefinition(ParseUnit&)");
    }

    this->definition = &pu;
}

ParseUnit& Variable::getDefinition() {
    return *definition;
}

void Variable::useOverload(ParseUnit& pu, Type& type) {
    if (overloads.count(type) != 0) {
        throw std::invalid_argument("Variable::useOverload(ParseUnit&, Type&)");
    }

    overloads[type] = &pu;
}

ParseUnit& Variable::getOverload(Type& type) {
    return *overloads.at(type);
}
