#ifndef LEEK_SLEEK_VARIABLE_H_DEFINED
#define LEEK_SLEEK_VARIABLE_H_DEFINED

#include <map>
#include "Type.hpp"

class ParseUnit;

enum class VariableType {
    REFERENCE,
    DEFINITION,
    OVERLOAD
};

class Variable {
    public:
        Variable();

        void useDefinition(ParseUnit& pu);
        ParseUnit& getDefinition();

        void useOverload(ParseUnit& pu, Type& type);
        ParseUnit& getOverload(Type& type);
    private:

        ParseUnit* definition;
        std::map<Type, ParseUnit*> overloads;
};

#endif
