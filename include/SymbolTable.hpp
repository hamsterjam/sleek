#ifndef LEEK_SLEEK_SYMBOL_TABLE_H_DEFINED
#define LEEK_SLEEK_SYMBOL_TABLE_H_DEFINED

#include "Variable.hpp"

#include <string>
#include <vector>
#include <map>

class SymbolTable;

class Symbol {
    public:
        Symbol();
        Symbol(VariableType type);
        ~Symbol();

        void aliasTo(Symbol& val);
        bool isDefinition();
        bool isOverload();
        Variable& getValue();

    protected:
        friend SymbolTable;
        void promoteToDefinition();
        void promoteToOverload();

    private:
        bool definition;
        bool overload;
        Variable* value;
};

class SymbolTable {
    public:
        typedef std::map<std::string, Symbol>::iterator iterator;

        SymbolTable();
        ~SymbolTable();

        SymbolTable* newScope();
        SymbolTable* exitScope();
        SymbolTable* getRoot();

        iterator get(std::string& key);
        iterator define(std::string& key);
        iterator overload(std::string& key);

        bool isFunctionExpression;
        bool isFunctionExpressionCT;

        bool isClassScope;

    private:
        friend Symbol;

        SymbolTable* root;
        SymbolTable* parent;
        std::vector<SymbolTable*> children;
        std::map<std::string, Symbol> data;

        iterator getRaw(std::string& key);
        void aliasAllForwardRefs(std::string& key, Symbol& val);
};

#endif
