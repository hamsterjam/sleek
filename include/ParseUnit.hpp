#ifndef LEEK_SLEEK_PARSE_UNIT_H_DEFINED
#define LEEK_SLEEK_PARSE_UNIT_H_DEFINED

#include "Token.hpp"
#include "AsyncQueue.hpp"
#include "Type.hpp"

#include <deque>
#include <list>

#include <thread>
#include <mutex>
#include <condition_variable>

class ParseUnit {
    public:
        void push(Token val);

        // Async actions
        void generate();
        void join();

        // This will block untill the type information is available
        Type getType();

    private:
        AsyncQueue<Token> iBuff;
        std::list<ParseUnit> children;
        Type type;

        std::thread th;

        bool typeReady;
        std::mutex type_m;
        std::condition_variable type_cv;

        void notifyTypeReady();
};

#endif
